package utils;

public class OtherFuncs {
    public static String generateString(){
        StringBuilder randString = new StringBuilder();

        int count = (int)(Math.random()*15 + 3);

        for(int i=0; i<count; i++)
            randString.append( (char)(int)((Math.random()*26) + 97));

        return randString.toString();
    }

    public static int generateInt(){
        return (int) Math.ceil(Math.random() * 100);
    }

    public static float generateFloat() {
        return (float) Math.ceil(Math.random() * 1000)/10.F;
    }
}
