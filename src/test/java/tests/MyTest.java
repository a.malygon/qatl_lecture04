package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.Dashboard;
import pages.LoginPage;
import pages.MainPage;
import pages.Products;
import utils.BaseTest;

public class MyTest extends BaseTest{

    private static WebDriver driver;
    private static String createdProductName;
    private static int createdProductAmount;
    private static float createdProductPrice;

    @Parameters("driver")
    @BeforeClass
    public void setUp(@Optional("chrome") String driver){
        this.driver = getConfiguredDriver(driver);
    }

    @DataProvider
    public Object[][] loginData(){
        return new Object[][]{
                new Object[] {
                        "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/",
                        "webinar.test@gmail.com",
                        "Xcg7299bnSmMuRLp9ITw"
                }
        };
    }

    @DataProvider
    public Object[][] mainPageData(){
        return new Object[][]{
                new Object[] {
                        "http://prestashop-automation.qatestlab.com.ua/",
                }
        };

    }

    @Test(dataProvider = "loginData")
    public void createProduct(String url, String email, String password){
        LoginPage loginPage = new LoginPage(driver);

        loginPage.open(url);
        loginPage.fillEmailInput(email);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginBtn();

        Dashboard dashboard = new Dashboard(driver);
        dashboard.selectProducts();

        Products products = new Products(driver);
        products.clickNewProductBtn();
        products.fillProductNameInput();
        products.fillProductAmountInput();
        products.fillProductPriceInput();
        products.checkActiveProductCheckbox();
        products.clickCloseAlertBtn();
        products.clickSubmitBtn();

        createdProductName = products.getCreatedProductName();
        createdProductAmount = products.getCreatedProductAmount();
        createdProductPrice = products.getCreatedProductPrice();
    }

    @Test(dataProvider = "mainPageData", dependsOnMethods = "createProduct")
    public void checkProductDisplay(String url){
        MainPage mainPage = new MainPage(driver);

        mainPage.open(url);
        mainPage.findProduct(createdProductName);

        String productTitle = driver.findElement(By.xpath("//h1[@itemprop='name']")).getText().toLowerCase();
        Assert.assertTrue(productTitle.matches(createdProductName), "Product name is not right");

        //String productAmount = driver.findElement(By.xpath("//input[@id='quantity_wanted']")).getAttribute("value");
        //Assert.assertTrue(productAmount.matches(Integer.toString(createdProductAmount)), "Product amount is not right");

        String productPrice = driver.findElement(By.xpath("//span[@itemprop='price']")).getAttribute("content");
        Assert.assertTrue(productPrice.matches(Float.toString(createdProductPrice)), "Product price is not right");

    }

    @AfterClass
    public void tearDown(){
        quiteDriver(driver);
    }
}
