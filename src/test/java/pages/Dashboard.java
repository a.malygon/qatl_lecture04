package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Dashboard {
    private WebDriver driver;

    private By catalog = By.id("subtab-AdminCatalog");
    private By categories = By.id("subtab-AdminCategories");
    private By newCategoryBtn = By.id("page-header-desc-category-new_category");
    private By products = By.id("subtab-AdminProducts");
    private By newProductBtn = By.id("page-header-desc-configuration-add");

    public Dashboard(WebDriver driver){
        this.driver = driver;
    }

    public void selectProducts(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(catalog));

        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(catalog)).build().perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(products));

        driver.findElement(products).click();

        wait.until(ExpectedConditions.elementToBeClickable(newProductBtn));
    }

    public void selectCategories(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(catalog));

        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(catalog)).build().perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(categories));

        driver.findElement(categories).click();

        wait.until(ExpectedConditions.elementToBeClickable(newCategoryBtn));
    }

}
