package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;

    private By emailInput = By.id("email");
    private By passwordInput = By.id("passwd");
    private By submitLoginBtn = By.name("submitLogin");

    //private String email = "webinar.test@gmail.com";
    //private String password = "Xcg7299bnSmMuRLp9ITw";

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public void open(String url){
        driver.get(url);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailInput));
    }

    public void fillEmailInput(String email){
        driver.findElement(emailInput).sendKeys(email);
    }

    public void fillPasswordInput(String password){
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginBtn(){
        driver.findElement(submitLoginBtn).click();
    }

}
