package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    private WebDriver driver;

    private By searchWidget = By.xpath("//input[@type='text']");
    private By submitBtn = By.xpath("//button[@type='submit']");
    private By searchResult = By.id("main");
    private By addToCartBtn = By.className("add-to-cart");

    public MainPage(WebDriver driver){
        this.driver = driver;
    }

    public void open(String url){
        driver.get(url);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchWidget));
    }

    public void findProduct(String productName){
        driver.findElement(searchWidget).sendKeys(productName);
        driver.findElement(submitBtn).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchResult));

        driver.findElement(By.xpath("//a[.='" + productName + "']")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(addToCartBtn));
    }


}
