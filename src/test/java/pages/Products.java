package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Products {

    private WebDriver driver;

    private By newProductBtn = By.id("page-header-desc-configuration-add");
    private By productNameField = By.id("form_step1_name_1");
    private By productAmountField = By.id("form_step1_qty_0_shortcut");
    private By productPriceField = By.id("form_step1_price_shortcut");
    private By activeProductCheckbox = By.className("switch-input ");
    private By alertCloseBtn = By.className("growl-close");
    private By submitBtn = By.cssSelector(".btn-group .btn[type='submit']");

    private String productName = utils.OtherFuncs.generateString();
    private int productAmount = utils.OtherFuncs.generateInt();
    private float productPrice = utils.OtherFuncs.generateFloat();

    public String getCreatedProductName(){
        return productName;
    }

    public int getCreatedProductAmount(){
        return productAmount;
    }

    public float getCreatedProductPrice(){
        return productPrice;
    }

    public Products(WebDriver driver){
        this.driver = driver;
    }

    public void clickNewProductBtn(){
        driver.findElement(newProductBtn).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(productNameField));
    }

    public void fillProductNameInput(){
        driver.findElement(productNameField).sendKeys(productName);
    }

    public void fillProductAmountInput(){
        WebElement elem = driver.findElement(productAmountField);
        elem.clear();
        elem.sendKeys(Integer.toString(productAmount));
    }

    public void fillProductPriceInput() {
        WebElement elem = driver.findElement(productPriceField);
        elem.clear();
        elem.sendKeys(Float.toString(productPrice));
    }


    public void checkActiveProductCheckbox() {
        driver.findElement(activeProductCheckbox).click();
    }

    public void clickCloseAlertBtn() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(alertCloseBtn));
        driver.findElement(alertCloseBtn).click();
    }

    public void clickSubmitBtn() {
        driver.findElement(submitBtn).click();
    }
}
